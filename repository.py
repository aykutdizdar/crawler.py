import sqlite3
import traceback

from sqlite3 import Error
from datetime import date

from configuration import configuration
from searchItem import search_item

class repository:
    def __init__(self):
        self.config = configuration()

    def set_configuration(self, configuration):
        self.config = configuration

    def db_create_database(self):
        # create a database connection to a SQLite database
        conn = None
        try:
            conn = sqlite3.connect(self.config.db_path)
            print(sqlite3.version)
        except:
            traceback.print_exc()

        finally:
            if conn:
                conn.close()

    def db_connect(self):
        con = sqlite3.connect(self.config.db_path)
        return con

    def db_create_table(self):
        sql = """ CREATE TABLE items(
            ID INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            price TEXT NULL,
            link TEXT NOT NULL,
            latitude TEXT NULL,
            longitude TEXT NULL,
            date TEXT NULL,
            type TEXT NULL,
            rooms TEXT NULL,
            floor TEXT NULL,
            area TEXT NULL,
            ilce TEXT NOT NULL,
            is_disabled CHAR(1) NOT NULL DEFAULT 'N',
            notes TEXT NULL,
            last_updated TEXT NULL)"""
        
        con = self.db_connect()
        con.execute(sql)
        con.close()

    def db_upsert_items(self, items):
        for item in items:
            self.db_upsert_item(item)

    def db_upsert_item(self, item):
        sql = """INSERT INTO items (ID,title,price,link,date,ilce) VALUES (?,?,?,?,?,?)
            ON CONFLICT(ID) DO UPDATE 
                SET title=?, price=?, link=?, latitude=?, longitude=?, 
                    date=?, type=?, rooms=?, floor=?, area=?, ilce=?, 
                    notes=?, last_update=?"""
        con = self.db_connect()
        params = (item.id, item.title, item.price, item.link, item.date, item.ilce, 
            item.title, item.price, item.link, item.latitude, item.longitude, item.date, 
            item.type, item.rooms, item.floor, item.area, item.ilce, item.notes,
            date.today().strftime("%Y%m%d"))

        con.execute(sql, params)
        con.commit()
        con.close()

    def db_insert_item(self, item):
        sql = "INSERT INTO items (ID,title,price,link,date,ilce,type) VALUES (?,?,?,?,?,?,?)"
        con = self.db_connect()
        params = (item.id, item.title, item.price, item.link, item.date, item.ilce, item.type)

        try:
            con.execute(sql, params)
            con.commit()
        except sqlite3.IntegrityError:
            print("Item exists > {0}".format(item.id))
        con.close()        

    def db_set_disabled(self, item, is_disabled):
        sql = "UPDATE items set is_disabled=? where id=?"
        con = self.db_connect()
        params = (is_disabled, item.id)
        con.execute(sql, params)
        con.commit()
        con.close()

    def db_get_all_items(self):
        sql = """SELECT ID, title, price, link, latitude, longitude, date, type, rooms, floor,
            area, ilce, is_disabled, notes, last_update FROM items WHERE is_disabled='N'"""    
        con = self.db_connect()
        items = []
        for row in con.execute(sql):
            item = search_item()
            item.id = row[0]
            item.title = row[1]
            item.price = row[2]
            item.link = row[3]
            item.latitude = row[4]
            item.longitude = row[5]
            item.date = row[6]
            item.type = row[7]
            item.rooms = row[8]
            item.floor = row[9]
            item.area = row[10]
            item.ilce = row[11]
            item.is_disabled = row[12]
            item.notes = row[13]
            item.last_update = row[14]

            items.append(item)
        return items