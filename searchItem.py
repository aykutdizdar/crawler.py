class search_item:
    def __init__(self, id = None, title = None, price = None, link = None, 
                latitude = None, longitude = None,
                date = None, type = None, rooms = None, floor = None, area = None, 
                ilce = None, list_url = None, is_disabled = None, notes = None,
                last_update = None):
        self.id = id
        self.title = title
        self.price = price
        self.link = link
        self.latitude = latitude
        self.longitude = longitude
        self.date = date
        self.type = type
        self.rooms = rooms
        self.floor = floor
        self.area = area
        self.ilce = ilce
        self.list_url = list_url
        self.is_disabled = is_disabled
        self.notes = notes
        self.last_update = last_update