class search_source_config:
    def __init__(self, search_uri_base = None, search_detail_uri_base = None,
        search_query = None, min_wait_between_requests = None, max_wait_between_requests = None, 
        max_results = None, type = None, update_details_if_blank = None):
        self.search_uri_base = search_uri_base
        self.search_districts = {}
        self.search_detail_uri_base = search_detail_uri_base
        self.search_query = search_query
        self.min_wait_between_requests = min_wait_between_requests
        self.max_wait_between_requests = max_wait_between_requests
        self.max_results = max_results
        self.type = type
        self.update_details_if_blank = update_details_if_blank

    def add_district(self, district, value):
        self.search_districts[district] = value
