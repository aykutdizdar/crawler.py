from contextlib import nullcontext
from distutils.command.config import config
from inspect import trace
import time
import os
import sys
import json
import traceback
import random
import pandas as pd
import urllib.request

# from selenium import webdriver
# from selenium.webdriver.remote import webelement
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver import ActionChains

# from selenium.webdriver.edge.service import Service
# from selenium.webdriver.edge.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options

from seleniumwire import webdriver

from bs4 import BeautifulSoup
from datetime import datetime

from searchItem import search_item
from configuration import configuration
from searchSourceConfig import search_source_config
from repository import repository

def get_realty_list(driver, search_source, repo):
    url = search_source.search_uri_base
    query = search_source.search_query
    ilceler = search_source.search_districts
    max_results = search_source.max_results

    total_results = 0

    for ilce in ilceler.keys():
        for semt in ilceler[ilce]:
            items = [];
            time_start = time.process_time()
            paging_offset = 0
            paginated_request_uri = "{0}-{1}-{2}?{3}".format(url, ilce, semt, query)

            while True:
                print("\r\n{0}".format(paginated_request_uri))

                try:
                    driver.get(paginated_request_uri)
                except:
                    traceback.print_exc()
                content = driver.page_source
                if content.find("Olağan dışı erişim") > 0:
                    print("Erişim bloklandı.")
                    return

                soup = BeautifulSoup(content, features="html.parser")

                # with urllib.request.urlopen(paginated_request_uri) as amz117:
                #     soup = BeautifulSoup(amz117.read())

                html_items = soup.find_all("tr", "searchResultsItem")
                if(len(html_items) == 0):
                    print("Hiç ilan bulunamadı > {0}/{1}".format(semt, ilce))
                    time.sleep(random.randint(search_source.min_wait_between_requests, search_source.max_wait_between_requests))
                    break

                for html_item in html_items:
                    html_title = html_item.find("a", "classifiedTitle")
                    
                    # Reklam satırları
                    if(html_title == None):
                        print("Reklam satırı atlandı")
                        continue
                    
                    item = search_item()
                    try:
                        item.id = html_item.get("data-id")
                        item.title = html_title.text.strip("\n ")
                        item.link = html_title.get("href")
                        item.price = html_item.find("td", "searchResultsPriceValue").text.strip("\n ")
                        item.date = html_item.find("td","searchResultsDateValue").text.strip("\n ").replace("\n\n", " ")
                        item.ilce = ilce
                        item.type = search_source.type
                        items.append(item)

                        # post items to database
                        repo.db_insert_item(item)
                    except:
                        traceback.print_exc()
                
                if(len(items) == 0):
                    break

                total_results += len(items)

                print("{0} sonuç getirildi/kaydedildi. Toplam İlan:{1} Toplam Süre: {2}".format(len(items), total_results, 
                    time.process_time()-time_start))
                items.clear()

                if soup.find("a", "prevNextBut") == None:
                    break
                if(total_results >= max_results):
                    break
            
                paging_offset += 50
                paginated_request_uri = "{0}-{1}-{2}?{3}&pagingOffset={4}".format(url, ilce, semt, query, paging_offset)
                
                time.sleep(random.randint(search_source.min_wait_between_requests, search_source.max_wait_between_requests))

def get_realty_detail(driver, search_source, repo, update_details_days):
    base_url = search_source.search_detail_uri_base
    
    items = repo.db_get_all_items()
    print("\r\n{0} ilan için detay sorgulaması yapılacak.".format(len(items)))

    time_start = time.process_time()

    count = 0
    for item in items:

        # if item.rooms is None:
        #     request_uri = "{0}{1}".format(base_url, item.link)
        #     print(request_uri)
        # else:
        #     print("{0} nolu ilan detayı zaten mevcut, atlanıyor.".format(item.id))
        #     continue

        if (item.last_update is None or 
            (datetime.now() - datetime.strptime(item.last_update, '%Y%m%d')).days >= update_details_days):
            request_uri = "{0}{1}".format(base_url, item.link)
            print("\r\n{0}".format(request_uri))
        else:
            print("\r\n{0} nolu ilanın son güncelleme tarihi üzerinden {1} gün geçmemiş, atlanıyor.".format(item.id, update_details_days))
            continue

        try:
            driver.get(request_uri)
        except:
            traceback.print_exc()
            continue

        content = driver.page_source
        if content.find("Protocol exception") > 0:
            print("Protocol bağlantı hatası.")
            break
        if content.find("Olağan dışı erişim") > 0:
            print("Erişim bloklandı.")
            break
        if content.find("Aradığınız sayfaya") > 0 and content.find("ulaşılamadı") > 0:
            print("İlan kaldırılmış > {0}".format(item.id))
            repo.db_set_disabled(item, 'Y')
            continue
        if content.find("Görüntülemek istediğiniz ilan yayında değildir") > 0:
            print("İlan kaldırılmış > {0}".format(item.id))
            repo.db_set_disabled(item, 'Y')
            continue
        
        soup = BeautifulSoup(content, features="html.parser")

        html_location = soup.find("div", id="gmap")
        if html_location == None:
            print("İlanda lokasyon bulunamadı > {0}".format(item.id))
        else:
            item.latitude = html_location.get("data-lat")
            item.longitude = html_location.get("data-lon")

        html_info = soup.find("ul", "classifiedInfoList").select("li")
        if html_info == None:
            print("İlan detayları bulunamadı > {0}".format(item.id))
            time.sleep(random.randint(search_source.max_wait_between_requests, search_source.max_wait_between_requests))
            continue

        #item_area = html_info.find_all("span")
        item.date = html_info[1].find("span").text.strip("\n\t ")
        item.area = html_info[4].find("span").text.strip("\n\t ")
        item.rooms = html_info[5].find("span").text.strip("\n\t ")
        item.floor = html_info[7].find("span").text.strip("\n\t ")

        repo.db_upsert_item(item)
        count+=1

        print("İlan detayları güncellendi > {0}".format(item.id))
        time.sleep(random.randint(search_source.max_wait_between_requests, search_source.max_wait_between_requests))

    print("Toplam {0} ilan detayı güncellendi. Süre: {1} secs".format(count, time.process_time()-time_start))

def driver_interceptor(request):
    request.headers["accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
    request.headers["accept-encoding"] = "gzip, deflate, br" 
    request.headers["accept-language"] = "en-US,en;q=0.9,tr;q=0.8" 
    request.headers["cache-control"] = "max-age=0"
    request.headers["content-type"] = "text/html; charset=utf-8"
    request.headers["referer"] = "https://www.google.com/"
    del request.headers["user-agent"] # Delete the header first 
    request.headers["user-agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.39"

def read_configuration():
    with open("config.json", "r") as f:
        configuration_file = json.loads(f.read())
    config = configuration(configuration_file["driver_path"], configuration_file["db_path"])
    config.update_detail_days = configuration_file["update_detail_days"]
    for search_source_config_item in configuration_file["search_sources"]:
        item = search_source_config()
        item.search_uri_base = search_source_config_item["search_uri_base"]
        item.search_detail_uri_base = search_source_config_item["search_detail_uri_base"]
        item.search_query = search_source_config_item["search_query"]
        item.min_wait_between_requests = search_source_config_item["min_wait_between_requests"]
        item.max_wait_between_requests = search_source_config_item["max_wait_between_requests"]
        item.max_results = search_source_config_item["max_results"]
        item.type = search_source_config_item["type"]
        for district in search_source_config_item["search_districts"]:
            item.add_district(district, search_source_config_item["search_districts"][district])
        config.add_search_source(item)
    return config

def init_driver(config):
    # Init browser support
    s = Service(config.driver_path)

    options = Options()
    # options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument("--log-level=2")
    options.add_argument("--ignore-certificate-errors-spki-list")
    options.add_argument("--ignore-ssl-errors")
    options.add_experimental_option('excludeSwitches', ['enable-logging'])

    driver = webdriver.Chrome(service = s, options = options )
    driver.request_interceptor = driver_interceptor

    return driver

def init_repository(config):
    repo = repository()
    repo.set_configuration(config)
    return repo

def main():

    config = read_configuration()
    driver = init_driver(config)
    repo = init_repository(config)
    # 0. Initialize database
    # db_create_database()
    # db_create_table()

    # 1. Get a realty list and insert into database
    get_realty_list(driver, config.search_sources[0], repo)

    # 2. Update details of previously inserted realties
    get_realty_detail(driver, config.search_sources[0], repo, config.update_detail_days)

    driver.quit()

if __name__ == "__main__":
    main()
