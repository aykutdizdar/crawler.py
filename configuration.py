from searchSourceConfig import search_source_config

class configuration:
    def __init__(self, driver_path = None, db_path = None, update_detail_days = None):
        self.driver_path = driver_path
        self.db_path = db_path
        self.update_detail_days = update_detail_days
        self.search_sources= []

    def add_search_source(self, search_source):
        self.search_sources.append(search_source)
